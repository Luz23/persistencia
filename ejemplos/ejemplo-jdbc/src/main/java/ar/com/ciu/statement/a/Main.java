package ar.com.ciu.statement.a;

import java.sql.Connection;
import java.sql.DriverManager;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.sql.Statement;

/**
 * 
 * Statement
 * 
 */
public class Main {

	private static Connection dbConnection = null;

	public static void main(String[] args) {
		try {
			//creo la conexion
			dbConnection = DriverManager.getConnection("jdbc:mysql://localhost:3306/ciupersistencia15", "admin", "admin");
			//Lo uso para crear sentencias sql
			Statement statement = dbConnection.createStatement();
			// select
			String query = "select * from titular";
			//ejecuto consulta sql
			ResultSet rs = statement.executeQuery(query);
			while (rs.next()) {
				int id = rs.getInt("id");
				String ape = rs.getString("apellido");
				String nom = rs.getString("nombre");
				System.out.println("");
				System.out.print("id: " + id);
				System.out.print(" , apellido: " + ape);
				System.out.print(" , nombre: " + nom);
			}
			//termina de ejecutar la query
			statement.close();
			//cierro la conexion para liberar los recursos
			dbConnection.close();
			
		} catch (SQLException e) {
			e.printStackTrace();
		
		}
	}

}
