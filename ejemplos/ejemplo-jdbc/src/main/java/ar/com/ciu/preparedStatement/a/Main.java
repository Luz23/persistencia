package ar.com.ciu.preparedStatement.a;

import java.sql.Connection;
import java.sql.DriverManager;
import java.sql.PreparedStatement;
import java.sql.SQLException;
import java.util.Date;

import ar.com.ciu.model.Titular;

/**
 * 
 * PreparedStatement.
 * 
 */
public class Main {

	private static Connection dbConnection = null;

	public static void main(String[] args) {
		try {
			
			Titular titular = new Titular("Juancho", "Roldan", new Date());
			
			Class.forName("com.mysql.cj.jdbc.Driver");
			
			dbConnection = DriverManager.getConnection("jdbc:mysql://localhost:3306/ciupersistencia15", "admin", "admin");
			String query="insert into titular (nombre, apellido, fecha_de_nacimiento) values (?, ?, ?)";
	        PreparedStatement preparedStatement;
	        preparedStatement = dbConnection.prepareStatement(query);
	        preparedStatement.setString(1, titular.getNombre());
	        preparedStatement.setString(2, titular.getApellido());
	        preparedStatement.setDate(3, new java.sql.Date(titular.getFechaDeNacimiento().getTime()));
	        preparedStatement.executeUpdate();
	        preparedStatement.close();
	        dbConnection.close();
	        System.out.println("HOLA ");
		} catch (ClassNotFoundException e) {
			System.out.println(e.getMessage());
		} catch (SQLException e) {
			System.out.println(e.getMessage());
		}		
	}

}
