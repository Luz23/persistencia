package ar.com.ciu.callableStatement.a;

import java.sql.CallableStatement;
import java.sql.Connection;
import java.sql.DriverManager;
import java.sql.SQLException;
import java.sql.Types;
import java.util.Date;

import ar.com.ciu.model.Telefono;
import ar.com.ciu.model.Titular;

public class Main {

	private static Connection dbConnection = null;

	public static void main(String[] args) {
		try {
			
			Titular titular = new Titular("Matias", "Gimenez", new Date());
			Telefono telefono = new Telefono(titular, 54, 11, 90871);
			
			Class.forName("com.mysql.cj.jdbc.Driver");
			dbConnection = DriverManager.getConnection("jdbc:mysql://localhost:3306/ciupersistencia15?autoReconnect=true&allowPublicKeyRetrieval=true&useSSL=false", "root", "admin");
			
			String query = "{call nuevo_titular_telefono(?,?,?,?,?,?,?)}";
	        CallableStatement callableStatement;
	        callableStatement = dbConnection.prepareCall(query);
	        callableStatement.setString(1, titular.getNombre());
	        callableStatement.setString(2, titular.getApellido());
	        callableStatement.setDate(3, new java.sql.Date(titular.getFechaDeNacimiento().getTime()));
	        callableStatement.setInt(4, telefono.getCodigoDePais());
	        callableStatement.setInt(5, telefono.getCodigoDeArea());
	        callableStatement.setInt(6, telefono.getNumero());
	        callableStatement.registerOutParameter(7, Types.INTEGER);
	        callableStatement.execute();
	        int idTitular = callableStatement.getInt(7);
	        System.out.println("Id Titular: " + idTitular);
	        callableStatement.close();
	        dbConnection.close();
		} catch (ClassNotFoundException e) {
			e.printStackTrace();
		} catch (SQLException e) {
			e.printStackTrace();
		} catch (Exception e) {
			e.printStackTrace();
		}
	}

}
