package ar.com.ciu.model;

public class Telefono {

	private Integer id;
	private Titular titular; 
	private Integer codigoDePais;
	private Integer codigoDeArea;
	private Integer numero;
	
	public Telefono(Titular titular, Integer codigoPais, Integer codigoArea, Integer numero) {
		super();
		this.titular = titular;
		this.codigoDePais = codigoPais;
		this.codigoDeArea = codigoArea;
		this.numero = numero;
	}
	
	public Integer getId() {
		return id;
	}
	public void setId(Integer id) {
		this.id = id;
	}
	public Titular getTitular() {
		return titular;
	}
	public void setTitular(Titular titular) {
		this.titular = titular;
	}
	public Integer getCodigoDePais() {
		return codigoDePais;
	}
	public void setCodigoDePais(Integer codigoDePais) {
		this.codigoDePais = codigoDePais;
	}
	public Integer getCodigoDeArea() {
		return codigoDeArea;
	}
	public void setCodigoDeArea(Integer codigoDeArea) {
		this.codigoDeArea = codigoDeArea;
	}
	public Integer getNumero() {
		return numero;
	}
	public void setNumero(Integer numero) {
		this.numero = numero;
	}


}
