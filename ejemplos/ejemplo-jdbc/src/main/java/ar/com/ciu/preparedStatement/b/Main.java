package ar.com.ciu.preparedStatement.b;

import java.sql.Connection;
import java.sql.DriverManager;
import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.sql.Statement;
import java.util.Date;

import ar.com.ciu.model.Titular;

/**
 * 
 * PreparedStatement que retorna id.
 * 
 *
 */
public class Main {

	private static Connection dbConnection = null;

	public static void main(String[] args) {
		try {
			
			Titular titular = new Titular("Camello", "El Plebello", new Date());
			
			Class.forName("com.mysql.cj.jdbc.Driver");
			dbConnection = DriverManager.getConnection("jdbc:mysql://localhost:3306/ciupersistencia15", "admin", "admin");
			String query = "insert into titular (nombre, apellido, fecha_de_nacimiento) values (?, ?, ?)";
	        PreparedStatement preparedStatement;
	        preparedStatement = dbConnection.prepareStatement(query, Statement.RETURN_GENERATED_KEYS); //Recupero la clave generada 
	        preparedStatement.setString(1, titular.getNombre());
	        preparedStatement.setString(2, titular.getApellido());
	        preparedStatement.setDate(3, new java.sql.Date(titular.getFechaDeNacimiento().getTime()));
	        preparedStatement.executeUpdate();
	        ResultSet rs = preparedStatement.getGeneratedKeys();
	        if (rs.next()) {
	            int idTitular = rs.getInt(1);
	            System.out.println("id titular: " + idTitular);
	        }
	        
	        // luego con ese id puedo insertar los telefonos, pero es ¿es atómico?
	        
		} catch (ClassNotFoundException e) {
			System.out.println(e.getMessage());
		} catch (SQLException e) {
			System.out.println(e.getMessage());
		}		
	}

}
