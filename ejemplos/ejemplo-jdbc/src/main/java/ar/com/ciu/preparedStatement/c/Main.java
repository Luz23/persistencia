package ar.com.ciu.preparedStatement.c;

import java.sql.Connection;
import java.sql.DriverManager;
import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.sql.Statement;
import java.util.Date;

import ar.com.ciu.model.Telefono;
import ar.com.ciu.model.Titular;

/**
 * 
 * PreparedStatement transaction
 * 
 *
 */
public class Main {

	private static Connection dbConnection = null;

	public static void main(String[] args) {
		try {
			Class.forName("com.mysql.cj.jdbc.Driver");
			dbConnection = DriverManager.getConnection("jdbc:mysql://localhost:3306/ciupersistencia15", "admin", "admin");

			dbConnection.setAutoCommit(false);

			int idTitular = -1;
			
			Titular titular = new Titular("Patricio", "Rey", new Date());
			Telefono telefono = new Telefono(titular, 54, 11, 99826451);

			String sqlTitular = "insert into titular (nombre, apellido, fecha_de_nacimiento) values (?, ?, ?)";
	        PreparedStatement preparedStatement;
	        preparedStatement = dbConnection.prepareStatement(sqlTitular, Statement.RETURN_GENERATED_KEYS);
	        preparedStatement.setString(1, titular.getNombre());
	        preparedStatement.setString(2, titular.getApellido());
	        preparedStatement.setDate(3, new java.sql.Date(titular.getFechaDeNacimiento().getTime()));
	        preparedStatement.executeUpdate();
	        ResultSet rs = preparedStatement.getGeneratedKeys();
	        if (rs.next()) {
	            idTitular = rs.getInt(1);
	            titular.setId(idTitular);
	        }

	        String sqlTelefono = "insert into telefono (id_titular, codigo_de_pais, codigo_de_area, numero) values (?, ?, ?, ?)";
	        preparedStatement = dbConnection.prepareStatement(sqlTelefono);
	        preparedStatement.setInt(1, telefono.getTitular().getId());
	        //preparedStatement.setInt(1, 789); //ROMPE PERO NO EJECUTO EL INSERT DEL TITULAR
	        preparedStatement.setInt(2, telefono.getCodigoDePais());
	        preparedStatement.setInt(3, telefono.getCodigoDeArea());
	        preparedStatement.setInt(4, telefono.getNumero());
	        preparedStatement.executeUpdate();
			
	        dbConnection.commit();
	        
		} catch (SQLException e) {
			e.printStackTrace();
		} catch (ClassNotFoundException e) {
			e.printStackTrace();
		}
	}

}
