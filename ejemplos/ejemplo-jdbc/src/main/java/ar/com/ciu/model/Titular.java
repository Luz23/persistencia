package ar.com.ciu.model;

import java.text.SimpleDateFormat;
import java.util.Date;

public class Titular {

		// atributos
	private Integer id;
	private String nombre;
	private String apellido;
	private Date fechaDeNacimiento;
	
		// constructor
	public Titular(String nombre, String apellido, Date fechaDeNacimiento) {
		super();
		this.nombre = nombre;
		this.apellido = apellido;
		this.fechaDeNacimiento = fechaDeNacimiento;
	}

		// metodos
	public String getFechaDeNacimientoSQL() {
		SimpleDateFormat sm = new SimpleDateFormat("yyyy-MM-dd");
		return sm.format(this.fechaDeNacimiento);
	}

		// gets y sets
	public Integer getId() {
		return id;
	}
	
	public void setId(Integer id) {
		this.id = id;
	}
	
	public String getNombre() {
		return nombre;
	}
	
	public void setNombre(String nombre) {
		this.nombre = nombre;
	}
	
	public String getApellido() {
		return apellido;
	}
	
	public void setApellido(String apellido) {
		this.apellido = apellido;
	}
	
	public Date getFechaDeNacimiento() {
		return fechaDeNacimiento;
	}
	
	public void setFechaDeNacimiento(Date fechaDeNacimiento) {
		this.fechaDeNacimiento = fechaDeNacimiento;
	}

}
