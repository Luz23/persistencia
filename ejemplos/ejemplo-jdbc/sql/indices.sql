use `ciupersistencia15`;

-- utilidades
delete from titular;
delete from telefono;


select count(*) from titular;

select count(*) from telefono;

select * from telefono order by id_titular;

select * from titular order by id desc limit 10;

select * from titular where apellido='oDxRLhjOHkYgJ ESPSvAIsLfAdJHWF' and nombre='ngYuRBjveFBaaDnNAqC';

-- indice
create index idx_titular_apellido_nombre on titular (apellido, nombre);

-- borrar indice
alter table titular drop index idx_titular_apellido_nombre;

-- ver el plan de ejecucion 
select 
	ti.nombre, ti.apellido, ti.fecha_de_nacimiento, te.codigo_de_pais, te.codigo_de_area, te.numero 
from 
	titular as ti 
		inner join telefono as te on (ti.id=te.id_titular)
where 
	apellido='oDxRLhjOHkYgJ ESPSvAIsLfAdJHWF';

