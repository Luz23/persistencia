
alter table titular add column cantidad_de_telefonos int null;

select * from telefono where id_titular=7;


delimiter $$
CREATE TRIGGER after_insert_telefono 
AFTER INSERT ON telefono for each row
begin
	declare cantidad int default 0;
	set cantidad = (select count(*) from telefono where id_titular=NEW.id_titular);
	update titular set cantidad_de_telefonos=cantidad where id=new.id_titular;
end$$

drop trigger after_insert_telefono;

insert into telefono (id_titular, codigo_de_pais, codigo_de_area, numero) values (7, 54, 11, 12345678);

