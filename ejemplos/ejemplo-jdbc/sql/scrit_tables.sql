create schema `ciupersistencia15` default character set latin1 collate latin1_spanish_ci;

use `ciupersistencia15`;

create table titular (
  id int not null auto_increment, 
  nombre varchar(256) not null,
  apellido varchar(256) not null, 
  fecha_de_nacimiento date not null, 
  primary key (id)
);

create table telefono (
  id int not null auto_increment, 
  id_titular int not null, 
  codigo_de_pais smallint not null, 
  codigo_de_area smallint not null,
  numero int not null,  
  primary key (id), 
  foreign key (id_titular) references titular (id)
);

select * from titular;
-- delete from titular where id<>1;

select * from telefono;
