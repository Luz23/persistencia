package ar.com.ciu.persistencia2.model;

public class Estudiante {
	    private int estudianteId;
	    private String estudianteNombre;
	    private String direccion;

	    public int getEstudianteId() {
	        return estudianteId;
	    }

	    public void setEstudianteId(int estudianteId) {
	        this.estudianteId = estudianteId;
	    }

	    public String getEstudianteNombre() {
	        return estudianteNombre;
	    }

	    public void setEstudianteNombre(String estudianteNombre) {
	        this.estudianteNombre = estudianteNombre;
	    }

	    public String getDireccion() {
	        return direccion;
	    }

	    public void setDireccion(String direccion) {
	        this.direccion = direccion;
	    }

}
