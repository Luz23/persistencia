package ar.com.ciu.persistencia2.model;

import org.hibernate.Session;
import org.hibernate.SessionFactory;
import org.hibernate.cfg.Configuration;


public class Main {
  public static void main(String[] args) {

    
    SessionFactory sessionFactory = new Configuration().configure("hibernate.cfg.xml")
			.addAnnotatedClass(Estudiante.class).buildSessionFactory();
	//Abro la session
	Session session = sessionFactory.openSession();
	
	try {
		  Estudiante estudiante = new Estudiante();
		    estudiante.setEstudianteNombre(" Landa");
		    estudiante.setDireccion("Madrid");
		    session.save(estudiante);
		//Abro la conexion con la base de datos
		session.beginTransaction();
		//Guardo en la base de datos
		session.save(estudiante);
		//Agarro la transaccion hasta el momento y con commit le digo que guarde los cambios
		session.getTransaction().commit();

		session.close();
		sessionFactory.close();
		System.out.println("estudiante creado !");

		} catch (Exception e) {
		e.printStackTrace();
		System.out.println("estudiante  no creado !");

		}
	
	
}
  
}
