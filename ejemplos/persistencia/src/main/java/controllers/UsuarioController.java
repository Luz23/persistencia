package controllers;

import org.hibernate.Session;
import org.hibernate.SessionFactory;
import org.hibernate.cfg.Configuration;

import ar.com.ciu.persistencia.Usuario;

public class UsuarioController {
	
	public String createUsuario(String nombre, String apellido, String direccion) {
		
		
		SessionFactory sessionFactory = new Configuration().configure("hibernate.cfg.xml")
				.addAnnotatedClass(Usuario.class).buildSessionFactory();
		//Abro la session
		Session session = sessionFactory.openSession();
		
		try {
			Usuario usuario = new Usuario(nombre, apellido, direccion);
			//Abro la conexion con la base de datos
			session.beginTransaction();
			//Guardo en la base de datos
			session.save(usuario);
			//Agarro la transaccion hasta el momento y con commit le digo que guarde los cambios
			session.getTransaction().commit();
			
			session.close();
			sessionFactory.close();

			return "usuario guardado con exito";
		} catch (Exception e) {
			e.printStackTrace();
			}
		
		return "No se pudo guardar el usuario";
		
	}
	
	public String deleteUsuario(int id) {
		SessionFactory sessionFactory = new Configuration().configure("hibernate.cfg.xml")
				.addAnnotatedClass(Usuario.class).buildSessionFactory();
		//Abro la session
		Session session = sessionFactory.openSession();
		
		try {
			//Abro la conexion con la base de datos
			session.beginTransaction();
			
			Usuario usuario = session.get(Usuario.class, id);
			//Guardo en la base de datos
			session.delete(usuario);;
			//Agarro la transaccion hasta el momento y con commit le digo que guarde los cambios
			session.getTransaction().commit();
			
			session.close();
			sessionFactory.close();
			return "usuario eliminado con exito";
		} catch (Exception e) {
			e.printStackTrace();
			}
		
		return "No se pudo eliminar el usuario";
	
	}
	
	public String updateUsuario(int id, String nombre) {
		SessionFactory sessionFactory = new Configuration().configure("hibernate.cfg.xml")
				.addAnnotatedClass(Usuario.class).buildSessionFactory();
		//Abro la session
		Session session = sessionFactory.openSession();
		
		try {
			//Abro la conexion con la base de datos
			session.beginTransaction();
			
			Usuario usuario = session.get(Usuario.class, id);
			//Guardo en la base de datos
			
			usuario.setNombre(nombre);
			
			session.update(usuario);
			//Agarro la transaccion hasta el momento y con commit le digo que guarde los cambios
			session.getTransaction().commit();
			
			session.close();
			sessionFactory.close();
			return "usuario actualizado con exito";
		} catch (Exception e) {
			e.printStackTrace();
			}
		
		return "No se pudo actualizar el usuario";
	}
	
	
	public String getUsuario(int id) {
		SessionFactory sessionFactory = new Configuration().configure("hibernate.cfg.xml")
				.addAnnotatedClass(Usuario.class).buildSessionFactory();
		//Abro la session
		Session session = sessionFactory.openSession();
		
		try {
			//Abro la conexion con la base de datos
			session.beginTransaction();
			
			Usuario usuario = session.get(Usuario.class, id);
			//Guardo en la base de datos
			
			//Agarro la transaccion hasta el momento y con commit le digo que guarde los cambios
			session.getTransaction().commit();
			
			session.close();
			return usuario.getApellido() + usuario.getNombre();
		} catch (Exception e) {
			e.printStackTrace();
			}
		
		return "No se pudo encontrar el usuario";
	}

}
