package ar.com.ciu.persistencia.market.service;

import ar.com.ciu.persistencia.market.dto.ClienteDTO;

public interface ClienteService {
	
	public ClienteDTO create(ClienteDTO clienteDTO);

	public ClienteDTO finById(Long idCliente);

	
	
	
//	public List<ClienteDTO> findAll();
//	
//	public ClienteDTO update(ClienteDTO clienteDTO);
//	
//	public void delete(Long idCliente);
	

}
