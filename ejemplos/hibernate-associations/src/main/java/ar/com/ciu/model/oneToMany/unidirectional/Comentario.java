package ar.com.ciu.model.oneToMany.unidirectional;

import java.util.Objects;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.Table;

import org.hibernate.annotations.GenericGenerator;
import org.hibernate.annotations.Type;

@Entity(name="Comentario")
@Table(name="comentario")
public class Comentario {

		// atributos
	private Integer id;
	private String detalle;
//	private Integer articuloId;

		// constructores
	public Comentario() {
		super();
	}

	public Comentario(String detalle) {
		this();
		this.detalle = detalle;
	}

		// metodos
	@Override
	public int hashCode() {
		return Objects.hash(this.id);
	}
	
	@Override
	public boolean equals(Object o) {
		if (o == this) return true;
		if (!(o instanceof Comentario)) {
			return false;
		}
		Comentario other = (Comentario) o;
		return Objects.equals(this.id, other.getId());
	}

		// gets y sets
	@Id
	@GeneratedValue(strategy=GenerationType.AUTO, generator="nativoDeBaseDeDatos")
	@GenericGenerator(name="nativoDeBaseDeDatos", strategy="native")
	public Integer getId() {
		return id;
	}

	public void setId(Integer id) {
		this.id = id;
	}

	@Column(length=2048, nullable=false)
	@Type(type="nstring")
	public String getDetalle() {
		return detalle;
	}

	public void setDetalle(String detalle) {
		this.detalle = detalle;
	}

	/*
	@Column(name="articulo_id")
	@Type(type="integer")
	public Integer getArticuloId() {
		return articuloId;
	}

	public void setArticuloId(Integer articulo_id) {
		this.articuloId = articulo_id;
	}
	*/

}
