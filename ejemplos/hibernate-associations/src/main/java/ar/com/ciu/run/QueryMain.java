package ar.com.ciu.run;

import org.apache.logging.log4j.LogManager;
import org.apache.logging.log4j.Logger;
import org.hibernate.Session;

import ar.com.ciu.hibernate.HibernateUtil;
import ar.com.ciu.model.oneToMany.bidirectional.Persona;

public class QueryMain {

	private static Logger logger = LogManager.getLogger(QueryMain.class);

	public static void main(String[] args) {

		Session session = HibernateUtil.getSessionFactory().openSession();

		Persona persona = session.get(Persona.class, new Integer(1));
		logger.info(persona.getNombre() + ", " + persona.getApellido());
		persona.getTelefonos().stream().forEach( t -> logger.info(t.getNumero()) );
		session.close();

		HibernateUtil.getSessionFactory().close();
		
	}

}
