package ar.com.ciu.run;

import org.hibernate.Session;

import ar.com.ciu.hibernate.HibernateUtil;
import ar.com.ciu.model.manyToMany.unidirectional.Palabra;

public class ManyToManyUnidirectionalMain {

	public static void main(String[] args) {

		Palabra p1 = new Palabra("Juan");
		p1.agregarLetra("J");
		p1.agregarLetra("u");
		p1.agregarLetra("a");
		p1.agregarLetra("n");

		Session session = HibernateUtil.getSessionFactory().openSession();

		try {
			session.beginTransaction();
			session.persist(p1);
			session.getTransaction().commit();
		} catch (Exception e) {
			e.printStackTrace();
			session.getTransaction().rollback();
		} finally {
			session.close();
		}

		HibernateUtil.getSessionFactory().close();
		
	}

}
