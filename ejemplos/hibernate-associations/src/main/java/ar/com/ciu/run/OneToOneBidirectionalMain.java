package ar.com.ciu.run;

import org.apache.logging.log4j.LogManager;
import org.apache.logging.log4j.Logger;
import org.hibernate.Session;

import ar.com.ciu.hibernate.HibernateUtil;
import ar.com.ciu.model.oneToOne.bidirectional.Linea;
import ar.com.ciu.model.oneToOne.bidirectional.Telefono;

public class OneToOneBidirectionalMain {

	private static final Logger logger = LogManager.getLogger(OneToOneBidirectionalMain.class);

	public static void main(String[] args) {
		Linea linea = new Linea("0221", "254698");
		Telefono tel = new Telefono("LG", "Lama", linea);

		Session session = HibernateUtil.getSessionFactory().openSession();
		try {
			session.beginTransaction();
			session.save(tel);
			session.getTransaction().commit();
			logger.info("Id telefono: " + tel.getId());
			Telefono tel2 = session.get(Telefono.class, tel.getId());
			logger.info(tel2.getMarca());
		} catch (Exception e) {
			e.printStackTrace();
			session.getTransaction().rollback();
		} finally {
			session.close();
		}


		Session session2 = HibernateUtil.getSessionFactory().openSession();
		Telefono tel2 = session2.find(Telefono.class, tel.getId());
		logger.info(tel2.getMarca());
		session2.close();

		HibernateUtil.getSessionFactory().close();
	}

}
