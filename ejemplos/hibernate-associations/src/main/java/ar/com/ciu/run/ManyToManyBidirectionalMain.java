package ar.com.ciu.run;

import org.hibernate.Session;

import ar.com.ciu.hibernate.HibernateUtil;
import ar.com.ciu.model.manyToMany.bidirectional.Ingrediente;
import ar.com.ciu.model.manyToMany.bidirectional.Receta;

public class ManyToManyBidirectionalMain {

	public static void main(String[] args) {
		Ingrediente harina = new Ingrediente("harina");
		Ingrediente azucar = new Ingrediente("azucar");
		Ingrediente sal = new Ingrediente("sal");
		Receta torta = new Receta("Torta");
		torta.agregarIngrediente(harina);
		torta.agregarIngrediente(azucar);

		Receta tortaFrita = new Receta("Torta frita");
		tortaFrita.agregarIngrediente(harina);
		tortaFrita.agregarIngrediente(sal);

		Session session = HibernateUtil.getSessionFactory().openSession();

		try {
			session.beginTransaction();
			session.persist(torta);
			session.persist(tortaFrita);
			session.getTransaction().commit();
		} catch (Exception e) {
			e.printStackTrace();
			session.getTransaction().rollback();
		} finally {
			session.close();
		}

		HibernateUtil.getSessionFactory().close();

	}

}
