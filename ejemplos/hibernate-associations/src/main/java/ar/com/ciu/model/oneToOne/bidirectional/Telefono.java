package ar.com.ciu.model.oneToOne.bidirectional;

import java.util.Objects;

import javax.persistence.CascadeType;
import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.OneToOne;
import javax.persistence.Table;

import org.hibernate.annotations.GenericGenerator;

@Entity(name="Telefono")
@Table(name="telefono")
public class Telefono {

		// atributos
	@Id
	@GeneratedValue(strategy=GenerationType.AUTO, generator="nativoDeBaseDeDatos")
	@GenericGenerator(name="nativoDeBaseDeDatos", strategy="native")
	private Long id;
	
	@Column
	private String marca;

	@Column
	private String modelo;

	@OneToOne(mappedBy="telefono", cascade=CascadeType.ALL)
	private Linea linea;

		// constructores
	public Telefono() {
		super();
	}
	
	public Telefono(String marca, String modelo, Linea linea) {
		this();
	    this.marca = marca;
	    this.modelo = modelo;
	    this.linea = linea;
	    linea.setTelefono(this);
	}

		// metodos
	@Override
	public int hashCode() {
		return Objects.hash(this.id);
	}
	
	@Override
	public boolean equals(Object o) {
		if (o == this) return true;
		if (!(o instanceof Telefono)) {
			return false;
		}
		Telefono other = (Telefono) o;
		return Objects.equals(this.id, other.getId());
	}

		// gets y sets
	public Long getId() {
		return id;
	}

	public void setId(Long id) {
		this.id = id;
	}

	public String getMarca() {
		return marca;
	}

	public void setMarca(String marca) {
		this.marca = marca;
	}

	public String getModelo() {
		return modelo;
	}

	public void setModelo(String modelo) {
		this.modelo = modelo;
	}

	public Linea getLinea() {
		return linea;
	}

	public void setLinea(Linea linea) {
		this.linea = linea;
	}

}
