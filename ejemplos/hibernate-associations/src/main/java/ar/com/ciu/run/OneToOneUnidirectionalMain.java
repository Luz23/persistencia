package ar.com.ciu.run;

import org.hibernate.Session;

import ar.com.ciu.hibernate.HibernateUtil;
import ar.com.ciu.model.oneToOne.unidirectional.Auto;
import ar.com.ciu.model.oneToOne.unidirectional.DetalleAuto;

public class OneToOneUnidirectionalMain {

	public static void main(String[] args) {
		DetalleAuto da = new DetalleAuto("Fox", (short)2011);
		Auto auto = new Auto("Volkswafen", da);

		Session session = HibernateUtil.getSessionFactory().openSession();

		try {
			session.beginTransaction();
			session.save(auto);
			session.getTransaction().commit();
		} catch (Exception e) {
			e.printStackTrace();
			session.getTransaction().rollback();
		} finally {
			session.close();
		}

		HibernateUtil.getSessionFactory().close();
	}

}
