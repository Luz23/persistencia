package ar.com.ciu.model.manyToMany.bidirectional;

import java.util.ArrayList;
import java.util.List;
import java.util.Objects;

import javax.persistence.CascadeType;
import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.Id;
import javax.persistence.ManyToMany;

@Entity(name="Receta")
public class Receta {

		// atributos
	@Id
	@GeneratedValue
	private Long id;

	@Column
	private String descripcion;

	@ManyToMany(cascade = {CascadeType.PERSIST, CascadeType.MERGE})
	private List<Ingrediente> ingredientes = new ArrayList<Ingrediente>();

		// constructores
	public Receta() {
		super();
	}

	public Receta(String descripcion) {
		this();
		this.descripcion = descripcion;
	}

		// metodos
	@Override
	public int hashCode() {
		return Objects.hash(this.id);
	}
	
	@Override
	public boolean equals(Object o) {
		if (o == this) return true;
		if (!(o instanceof Receta)) {
			return false;
		}
		Receta other = (Receta) o;
		return Objects.equals(this.id, other.getId());
	}

	public void agregarIngrediente(Ingrediente ingrediente) {
		this.ingredientes.add(ingrediente);
		ingrediente.getRecetas().add(this);
	}

		// gets y sets
	public Long getId() {
		return id;
	}

	public void setId(Long id) {
		this.id = id;
	}

	public String getDescripcion() {
		return descripcion;
	}

	public void setDescripcion(String descripcion) {
		this.descripcion = descripcion;
	}

	public List<Ingrediente> getIngredientes() {
		return ingredientes;
	}

	public void setIngredientes(List<Ingrediente> ingredientes) {
		this.ingredientes = ingredientes;
	}

}
