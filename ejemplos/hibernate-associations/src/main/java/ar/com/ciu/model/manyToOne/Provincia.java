package ar.com.ciu.model.manyToOne;

import java.util.Objects;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.Table;

import org.hibernate.annotations.GenericGenerator;

@Entity(name="Provincia")
@Table(name="provincia")
public class Provincia {

		// atributos
	private Integer id;
	private String descripcion;

		// constructores
	public Provincia() {
		super();
	}

	public Provincia(String descripcion) {
		this();
		this.descripcion = descripcion;
	}

		// metodos
	@Override
	public int hashCode() {
		return Objects.hash(this.id);
	}
	
	@Override
	public boolean equals(Object o) {
		if (o == this) return true;
		if (!(o instanceof Provincia)) {
			return false;
		}
		Provincia other = (Provincia) o;
		return Objects.equals(this.id, other.getId());
	}
	
		// gets y sets
	@Id
	@GeneratedValue(strategy=GenerationType.AUTO, generator="nativoDeBaseDeDatos")
	@GenericGenerator(name="nativoDeBaseDeDatos", strategy="native")
	public Integer getId() {
		return id;
	}
	
	public void setId(Integer id) {
		this.id = id;
	}

	public void setDescripcion(String descripcion) {
		this.descripcion = descripcion;
	}

	@Column(length=255, nullable=false)
	public String getDescripcion() {
		return descripcion;
	}

}
