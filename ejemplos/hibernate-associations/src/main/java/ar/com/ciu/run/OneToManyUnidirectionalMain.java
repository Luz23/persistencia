package ar.com.ciu.run;

import org.apache.logging.log4j.LogManager;
import org.apache.logging.log4j.Logger;
import org.hibernate.Session;

import ar.com.ciu.hibernate.HibernateUtil;
import ar.com.ciu.model.oneToMany.unidirectional.Articulo;
import ar.com.ciu.model.oneToMany.unidirectional.Comentario;

public class OneToManyUnidirectionalMain {

	private static final Logger logger = LogManager.getLogger(OneToManyUnidirectionalMain.class);

	public static void main(String[] args) {

		Articulo a1 = new Articulo("El ser humano nunca llego a la luna");
		Comentario c1 = new Comentario("fue todo invento yankee");
		Comentario c2 = new Comentario("fue todo una puesta en escena....como las torres...");
		Comentario c3 = new Comentario("son todos caretas!!! llegamos porque somos re piolas!");
		a1.agregarComentario(c1);
		a1.agregarComentario(c2);
		a1.agregarComentario(c3);
	
		Session session = HibernateUtil.getSessionFactory().openSession();

		try {
			session.beginTransaction();
			session.save(a1);
			session.flush();
			a1.getComentarios().remove(c3);
			session.getTransaction().commit();
		} catch (Exception e) {
			e.printStackTrace();
			session.getTransaction().rollback();
		} finally {
			session.close();
		}

		//
		Session session2 = HibernateUtil.getSessionFactory().openSession();
		Articulo a2 = session2.get(Articulo.class, a1.getId());
		session2.close();

		HibernateUtil.getSessionFactory().close();

		a2.getComentarios().stream().forEach(c -> logger.info(c.getDetalle()) );

	}

}
