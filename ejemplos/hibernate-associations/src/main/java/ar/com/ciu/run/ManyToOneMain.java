package ar.com.ciu.run;

import org.hibernate.Session;

import ar.com.ciu.hibernate.HibernateUtil;
import ar.com.ciu.model.manyToOne.Equipo;
import ar.com.ciu.model.manyToOne.Provincia;

public class ManyToOneMain {

	public static void main(String[] args) {

		Equipo e1 = new Equipo("River");


		Provincia p1 = new Provincia("Buenos Aires");
		e1.setProvincia(p1);

		Session session = HibernateUtil.getSessionFactory().getCurrentSession();
        //session.beginTransaction();

		try {
			session.beginTransaction();
			session.persist(p1);
			session.persist(e1);
		
			
			session.getTransaction().commit();
		} catch (Exception e) {
			e.printStackTrace();
			session.getTransaction().rollback();
		} finally {
			session.close();
		}

		HibernateUtil.getSessionFactory().close();
		
	}

}
