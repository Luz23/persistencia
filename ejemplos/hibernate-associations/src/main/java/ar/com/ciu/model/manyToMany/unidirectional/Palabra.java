package ar.com.ciu.model.manyToMany.unidirectional;

import java.util.ArrayList;
import java.util.List;
import java.util.Objects;

import javax.persistence.CascadeType;
import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.Id;
import javax.persistence.ManyToMany;

@Entity(name="Palabra")
public class Palabra {

		// atributos
	@Id
	@GeneratedValue
	private Long id;

	@Column
	private String descripcion;

	@ManyToMany(cascade = {CascadeType.PERSIST, CascadeType.MERGE})
	private List<Letra> letras = new ArrayList<>();

		// constructor
	public Palabra() {
		super();
	}

	public Palabra(String descripcion) {
		this();
		this.descripcion = descripcion;
	}

		// metodos
	@Override
	public int hashCode() {
		return Objects.hash(this.id);
	}
	
	@Override
	public boolean equals(Object o) {
		if (o == this) return true;
		if (!(o instanceof Palabra)) {
			return false;
		}
		Palabra other = (Palabra) o;
		return Objects.equals(this.id, other.getId());
	}

	public void agregarLetra(String letra) {
		this.letras.add(new Letra(letra));
	}

		// gets y sets
	public Long getId() {
		return id;
	}

	public void setId(Long id) {
		this.id = id;
	}

	public List<Letra> getLetras() {
		return letras;
	}

	public void setLetras(List<Letra> letras) {
		this.letras = letras;
	}

	public String getDescripcion() {
		return descripcion;
	}

	public void setDescripcion(String descripcion) {
		this.descripcion = descripcion;
	}

}
