package ar.com.ciu.model.oneToOne.unidirectional;

import java.util.Objects;

import javax.persistence.CascadeType;
import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.JoinColumn;
import javax.persistence.OneToOne;
import javax.persistence.Table;

import org.hibernate.annotations.GenericGenerator;

@Entity(name="Auto")
@Table(name="auto")
public class Auto {

		// atributos
	@Id
	@GeneratedValue(strategy=GenerationType.AUTO, generator="nativoDeBaseDeDatos")
	@GenericGenerator(name="nativoDeBaseDeDatos", strategy="native")
    private Long id;

    @Column
    private String marca;

    @OneToOne(cascade=CascadeType.ALL)
    @JoinColumn(name = "detalle_id")
    private DetalleAuto detalle;

    	// constructores
    public Auto() {
    	super();
    }

    public Auto(String marca, DetalleAuto detalle) {
    	this();
        this.marca = marca;
        this.detalle = detalle;
    }

		// metodos
	@Override
	public int hashCode() {
		return Objects.hash(this.id);
	}
	
	@Override
	public boolean equals(Object o) {
		if (o == this) return true;
		if (!(o instanceof Auto)) {
			return false;
		}
		Auto other = (Auto) o;
		return Objects.equals(this.id, other.getId());
}

    	// gets y sets
	public Long getId() {
		return id;
	}

	public void setId(Long id) {
		this.id = id;
	}

	public String getMarca() {
		return marca;
	}

	public void setMarca(String marca) {
		this.marca = marca;
	}

	public DetalleAuto getDetalle() {
		return detalle;
	}

	public void setDetalle(DetalleAuto detalle) {
		this.detalle = detalle;
	}

}
