package ar.com.ciu.model.oneToOne.unidirectional;

import java.util.Objects;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.Table;

import org.hibernate.annotations.GenericGenerator;

@Entity(name="DetalleAuto")
@Table(name="detalle_auto")
public class DetalleAuto {

		// atributos
	@Id
	@GeneratedValue(strategy=GenerationType.AUTO, generator="nativoDeBaseDeDatos")
	@GenericGenerator(name="nativoDeBaseDeDatos", strategy="native")
	private Long id;

	@Column(name="modelo", length=255, nullable=false)
	private String modelo;

	@Column(name="anio_fabricacion")
	private Short anioFabricacion;

		// constructores
	public DetalleAuto() {
		super();
	}

	public DetalleAuto(String modelo, Short anioFabricacion) {
		this();
		this.modelo = modelo;
		this.anioFabricacion = anioFabricacion;
	}

		// metodos
	@Override
	public int hashCode() {
		return Objects.hash(this.id);
	}
	
	@Override
	public boolean equals(Object o) {
		if (o == this) return true;
		if (!(o instanceof DetalleAuto)) {
			return false;
		}
		DetalleAuto other = (DetalleAuto) o;
		return Objects.equals(this.id, other.getId());
	}

		// gets y sets
	public Long getId() {
		return id;
	}

	public void setId(Long id) {
		this.id = id;
	}

	public String getModelo() {
		return modelo;
	}

	public void setModelo(String modelo) {
		this.modelo = modelo;
	}

	public Short getAnioFabricacion() {
		return anioFabricacion;
	}

	public void setAnioFabricacion(Short anioFabricacion) {
		this.anioFabricacion = anioFabricacion;
	}

}
