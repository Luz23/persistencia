package ar.com.ciu.model.manyToMany.unidirectional;

import java.util.Objects;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.Id;

@Entity(name="Letra")
public class Letra {

		// atributos
	@Id
	@GeneratedValue
	private Long id;
	
	@Column
	private String descripcion;

		// constructores
	public Letra() {
		super();
	}

	public Letra(String descripcion) {
		this();
		this.descripcion = descripcion;
	}

		// metodos
	@Override
	public int hashCode() {
		return Objects.hash(this.id);
	}
	
	@Override
	public boolean equals(Object o) {
		if (o == this) return true;
		if (!(o instanceof Letra)) {
			return false;
		}
		Letra other = (Letra) o;
		return Objects.equals(this.id, other.getId());
	}

		// gets y sets
	public Long getId() {
		return id;
	}

	public void setId(Long id) {
		this.id = id;
	}

	public String getDescripcion() {
		return descripcion;
	}

	public void setDescripcion(String descripcion) {
		this.descripcion = descripcion;
	}

}
