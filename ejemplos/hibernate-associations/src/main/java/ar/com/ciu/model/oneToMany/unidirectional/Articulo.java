package ar.com.ciu.model.oneToMany.unidirectional;

import java.util.ArrayList;
import java.util.List;
import java.util.Objects;

import javax.persistence.CascadeType;
import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.FetchType;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.JoinColumn;
import javax.persistence.OneToMany;
import javax.persistence.Table;

import org.hibernate.annotations.GenericGenerator;
import org.hibernate.annotations.Type;

@Entity(name="Articulo")
@Table(name="articulo")
public class Articulo {

		// atributos
	private Integer id;
	private String descripcion;
	private List<Comentario> comentarios;

		// constructor
	public Articulo() {
		super();
	}

	public Articulo(String descripcion) {
		this();
		this.descripcion = descripcion;
		this.comentarios = new ArrayList<>();
	}

		// metodos
	@Override
	public int hashCode() {
		return Objects.hash(this.id);
	}
	
	@Override
	public boolean equals(Object o) {
		if (o == this) return true;
		if (!(o instanceof Articulo)) {
			return false;
		}
		Articulo other = (Articulo) o;
		return Objects.equals(this.id, other.getId());
	}

	public void agregarComentario(Comentario c) {
		this.comentarios.add(c);
	}

		// gets y sets
	@Id
	@GeneratedValue(strategy=GenerationType.AUTO, generator="nativoDeBaseDeDatos")
	@GenericGenerator(name="nativoDeBaseDeDatos", strategy="native")
	public Integer getId() {
		return id;
	}

	public void setId(Integer id) {
		this.id = id;
	}

	@Column(length=255, nullable=false)
	@Type(type="nstring")
	public String getDescripcion() {
		return descripcion;
	}

	public void setDescripcion(String descripcion) {
		this.descripcion = descripcion;
	}

    @OneToMany(cascade=CascadeType.ALL, orphanRemoval=true, fetch=FetchType.EAGER)
    @JoinColumn(name = "articulo_id") // performante
	public List<Comentario> getComentarios() {
		return comentarios;
	}

	public void setComentarios(List<Comentario> comentarios) {
		this.comentarios = comentarios;
	}

}
