package ar.com.ciu.model.manyToMany.bidirectional;

import java.util.ArrayList;
import java.util.List;
import java.util.Objects;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.Id;
import javax.persistence.ManyToMany;

@Entity(name="Ingrediente")
public class Ingrediente {

		// atributos
	@Id
	@GeneratedValue
	private Long id;
	
	@Column
	private String descripcion;

	@ManyToMany(mappedBy="ingredientes")
	private List<Receta> recetas = new ArrayList<Receta>();

		// constructores
	public Ingrediente() {
		super();
	}

	public Ingrediente(String descripcion) {
		this();
		this.descripcion = descripcion;
	}

		// metodos
	@Override
	public int hashCode() {
		return Objects.hash(this.id);
	}
	
	@Override
	public boolean equals(Object o) {
		if (o == this) return true;
		if (!(o instanceof Ingrediente)) {
			return false;
		}
		Ingrediente other = (Ingrediente) o;
		return Objects.equals(this.id, other.getId());
	}

		// gets y sets
	public Long getId() {
		return id;
	}

	public void setId(Long id) {
		this.id = id;
	}

	public String getDescripcion() {
		return descripcion;
	}

	public void setDescripcion(String descripcion) {
		this.descripcion = descripcion;
	}

	public List<Receta> getRecetas() {
		return recetas;
	}

	public void setRecetas(List<Receta> recetas) {
		this.recetas = recetas;
	}

}
