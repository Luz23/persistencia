package ar.com.ciu.model.oneToMany.bidirectional;

import java.util.Objects;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.ManyToOne;
import javax.persistence.Table;

import org.hibernate.annotations.GenericGenerator;
import org.hibernate.annotations.Type;

@Entity(name="Telefono")
@Table(name="telefono")
public class Telefono {

		// telefono
	private Integer id;
	private String numero;
	private Persona persona;

		// constructores
	public Telefono() {
		super();
	}

	public Telefono(String numero) {
		this();
		this.numero = numero;
	}

		// metodos
	@Override
	public int hashCode() {
		return Objects.hash(this.id);
	}
	
	@Override
	public boolean equals(Object o) {
		if (o == this) return true;
		if (!(o instanceof Telefono)) {
			return false;
		}
		Telefono other = (Telefono) o;
		return Objects.equals(this.id, other.getId());
}

		// gets y sets
	@Id
	@GeneratedValue(strategy=GenerationType.AUTO, generator="nativoDeBaseDeDatos")
	@GenericGenerator(name="nativoDeBaseDeDatos", strategy="native")
	public Integer getId() {
		return id;
	}
	
	public void setId(Integer id) {
		this.id = id;
	}

	@Column(length=255, nullable=false)
	@Type(type="nstring")
	public String getNumero() {
		return numero;
	}

	public void setNumero(String numero) {
		this.numero = numero;
	}

	@ManyToOne
	public Persona getPersona() {
		return persona;
	}

	public void setPersona(Persona persona) {
		this.persona = persona;
	}

}
