package ar.com.ciu.model.oneToMany.bidirectional;

import java.util.ArrayList;
import java.util.List;
import java.util.Objects;

import javax.persistence.CascadeType;
import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.OneToMany;
import javax.persistence.Table;

import org.hibernate.annotations.GenericGenerator;
import org.hibernate.annotations.Type;

@Entity(name="Persona")
@Table(name="persona")
public class Persona {

		// atributos
	private Integer id;
	private String apellido;
	private String nombre;
    private List<Telefono> telefonos;

		// constructores
	public Persona() {
		super();
	}

    public Persona(String apellido, String nombre) {
    	this();
    	this.apellido = apellido;
    	this.nombre = nombre;
    	this.telefonos = new ArrayList<>();
    }

		// metodos
	@Override
	public int hashCode() {
		return Objects.hash(this.id);
	}
	
	@Override
	public boolean equals(Object o) {
		if (o == this) return true;
		if (!(o instanceof Persona)) {
			return false;
		}
		Persona other = (Persona) o;
		return Objects.equals(this.id, other.getId());
	}

	public void agregarTelefono(Telefono telefono) {
    	this.telefonos.add(telefono);
    	telefono.setPersona(this);
    }

		// gets y sets
	@Id
	@GeneratedValue(strategy=GenerationType.AUTO, generator="nativoDeBaseDeDatos")
	@GenericGenerator(name="nativoDeBaseDeDatos", strategy="native")
	public Integer getId() {
		return id;
	}
	
	public void setId(Integer id) {
		this.id = id;
	}

	@Column(length=255, nullable=false)
	@Type(type="string")
	public String getApellido() {
		return apellido;
	}

	public void setApellido(String apellido) {
		this.apellido = apellido;
	}

	@Column(length=255, nullable=false)
	@Type(type="nstring")
	public String getNombre() {
		return nombre;
	}

	public void setNombre(String nombre) {
		this.nombre = nombre;
	}

    @OneToMany(mappedBy = "persona", cascade = CascadeType.ALL, orphanRemoval = true)
	public List<Telefono> getTelefonos() {
		return telefonos;
	}

	public void setTelefonos(List<Telefono> telefonos) {
		this.telefonos = telefonos;
	}

}
