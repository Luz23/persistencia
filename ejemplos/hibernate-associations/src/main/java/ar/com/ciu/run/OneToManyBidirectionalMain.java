package ar.com.ciu.run;

import org.hibernate.Session;

import ar.com.ciu.hibernate.HibernateUtil;
import ar.com.ciu.model.oneToMany.bidirectional.Persona;
import ar.com.ciu.model.oneToMany.bidirectional.Telefono;

public class OneToManyBidirectionalMain {

	public static void main(String[] args) {
		
		Persona pablito = new Persona("Pablo", "Clavo");
		Telefono t1 = new Telefono("+5401166058725");
		Telefono t2 = new Telefono("+54022345714");
		pablito.agregarTelefono(t1);
		pablito.agregarTelefono(t2);
		
		Session session = HibernateUtil.getSessionFactory().openSession();

		try {
			session.beginTransaction();
			session.save(pablito);
			session.getTransaction().commit();
		} catch (Exception e) {
			e.printStackTrace();
			session.getTransaction().rollback();
		} finally {
			session.close();
		}

		HibernateUtil.getSessionFactory().close();
	}

}
