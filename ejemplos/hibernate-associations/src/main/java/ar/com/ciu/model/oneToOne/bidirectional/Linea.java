package ar.com.ciu.model.oneToOne.bidirectional;

import java.util.Objects;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.ForeignKey;
import javax.persistence.GeneratedValue;
import javax.persistence.Id;
import javax.persistence.JoinColumn;
import javax.persistence.OneToOne;
import javax.persistence.Table;

@Entity(name="Linea")
@Table(name="linea")
public class Linea {

		// atributos
	@Id
	@GeneratedValue
	private Long id;

	@Column
	private String area;
	
	@Column
	private String numero;

	@OneToOne
	@JoinColumn(name = "telefono_id", foreignKey=@ForeignKey(name="linea_id_fk"))
	private Telefono telefono;

		// constructores
	public Linea() {
		super();
	}

	public Linea(String area, String numero) {
		this();
		this.area = area;
		this.numero = numero;
	}

		// metodos
	@Override
	public int hashCode() {
		return Objects.hash(this.id);
	}
	
	@Override
	public boolean equals(Object o) {
		if (o == this) return true;
		if (!(o instanceof Linea)) {
			return false;
		}
		Linea other = (Linea) o;
		return Objects.equals(this.id, other.getId());
	}

		// gets y sets
	public Long getId() {
		return id;
	}

	public void setId(Long id) {
		this.id = id;
	}

	public String getArea() {
		return area;
	}

	public void setArea(String area) {
		this.area = area;
	}

	public String getNumero() {
		return numero;
	}

	public void setNumero(String numero) {
		this.numero = numero;
	}

	public Telefono getTelefono() {
		return telefono;
	}

	public void setTelefono(Telefono telefono) {
		this.telefono = telefono;
	}

}
