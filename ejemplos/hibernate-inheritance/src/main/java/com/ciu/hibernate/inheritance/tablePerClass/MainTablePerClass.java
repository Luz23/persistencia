package com.ciu.hibernate.inheritance.tablePerClass;

import java.math.BigDecimal;
import java.util.List;

import javax.persistence.Query;

import org.apache.logging.log4j.LogManager;
import org.apache.logging.log4j.Logger;
import org.hibernate.Session;

import com.ciu.hibernate.util.HibernateUtil;

/**
 * 
 * Descripcion: Todas las clases tienen su tabla.
 * Observaciones: No puedo usar id nativo de BD.
 * Desventaja: Muchos UNION si son muchas subclases.
 * 
 *
 */
public class MainTablePerClass {

	private static Logger logger = LogManager.getLogger(MainTablePerClass.class);

	public static void main(String[] args) {

		Cuenta cd = new CuentaDebito("Ciro Pertussi", new BigDecimal(24500), new BigDecimal(400.95));
		Cuenta cc = new CuentaCredito("Andres Ciro", new BigDecimal(38900), new BigDecimal(679.12));
	
		Session session = HibernateUtil.getSessionFactory().openSession();

		try {
			session.beginTransaction();
			session.persist(cd);
			session.persist(cc);
			session.getTransaction().commit();
			Query query = session.createQuery("from Cuenta"); 
			@SuppressWarnings("unchecked")
			List<Cuenta> cuentas = query.getResultList();
			cuentas.stream().forEach( c -> logger.info(c.getTitular()) );
		} catch (Exception e) {
			e.printStackTrace();
			session.getTransaction().rollback();
		} finally {
			session.close();
		}

		HibernateUtil.getSessionFactory().close();
	}

}
