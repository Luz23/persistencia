package com.ciu.hibernate.inheritance.tablePerClass;

import java.math.BigDecimal;

import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.Id;
import javax.persistence.Inheritance;
import javax.persistence.InheritanceType;

@Inheritance(strategy = InheritanceType.TABLE_PER_CLASS)
@Entity
public abstract class Cuenta {

		// atributos
	@Id
	@GeneratedValue
	private Long id;

	private String titular;

	private BigDecimal balance;

		// constructores
	public Cuenta() {
		super();
	}

	public Cuenta(String titular, BigDecimal balance) {
		this();
		this.titular = titular;
		this.balance = balance;
	}

		// gets y sets
	public Long getId() {
		return id;
	}

	public void setId(Long id) {
		this.id = id;
	}

	public String getTitular() {
		return titular;
	}

	public void setTitular(String titular) {
		this.titular = titular;
	}

	public BigDecimal getBalance() {
		return balance;
	}

	public void setBalance(BigDecimal balance) {
		this.balance = balance;
	}

}
