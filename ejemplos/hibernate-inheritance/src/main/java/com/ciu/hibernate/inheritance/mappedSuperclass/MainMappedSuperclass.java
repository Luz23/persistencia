package com.ciu.hibernate.inheritance.mappedSuperclass;

import java.math.BigDecimal;
import java.util.List;

import javax.persistence.Query;

import org.apache.logging.log4j.LogManager;
import org.apache.logging.log4j.Logger;
import org.hibernate.Session;

import com.ciu.hibernate.util.HibernateUtil;

/**
 * 
 * Descripcion: La herencia es visible solo del lado del modelo (Objetos) y
 *  cada tabla contiene ambos atributos, los de la super clase y los de la 
 *    clase concreta. 
 * 
 *
 */
public class MainMappedSuperclass {

	private static Logger logger = LogManager.getLogger(MainMappedSuperclass.class);

	public static void main(String[] args) {

		Cuenta cd = new CuentaDebito("Ciro Pertussi", new BigDecimal(24500), new BigDecimal(400.95));
		Cuenta cc = new CuentaCredito("Andres Ciro", new BigDecimal(38900), new BigDecimal(679.12));
	
		Session session = HibernateUtil.getSessionFactory().openSession();

		try {
			session.beginTransaction();
			session.persist(cd);
			session.persist(cc);
			session.getTransaction().commit();
			Query query = session.createQuery("from CuentaCredito");
			@SuppressWarnings("unchecked")
			List<Cuenta> cuentas = query.getResultList();
			cuentas.stream().forEach( c -> logger.info(c.getTitular()) );
		} catch (Exception e) {
			e.printStackTrace();
			session.getTransaction().rollback();
		} finally {
			session.close();
		}

		HibernateUtil.getSessionFactory().close();
	}

}
