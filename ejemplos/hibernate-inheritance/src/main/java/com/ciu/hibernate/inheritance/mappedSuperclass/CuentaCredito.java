package com.ciu.hibernate.inheritance.mappedSuperclass;

import java.math.BigDecimal;

import javax.persistence.Entity;

@Entity(name="CuentaCredito")
public class CuentaCredito extends Cuenta {

		// atributos
	private BigDecimal limiteDeCredito;

		// constructores
	public CuentaCredito() {
		super();
	}
	
	public CuentaCredito(String titular, BigDecimal balance, BigDecimal limiteDeCredito) {
		super(titular, balance);
		this.limiteDeCredito = limiteDeCredito;
	}

		// gets y sets
	public BigDecimal getLimiteDeCredito() {
		return limiteDeCredito;
	}

	public void setLimiteDeCredito(BigDecimal limiteDeCredito) {
		this.limiteDeCredito = limiteDeCredito;
	}

}
