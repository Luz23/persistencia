package com.ciu.hibernate.inheritance.singleTable;

import java.math.BigDecimal;
import java.util.List;

import javax.persistence.Query;

import org.apache.logging.log4j.LogManager;
import org.apache.logging.log4j.Logger;
import org.hibernate.Session;

import com.ciu.hibernate.util.HibernateUtil;

/**
 * 
 * Descripcion: Esta estrategia mapea todas las subclases a una unica tabla de la base de datos.
 * Observaciones: Comparten el id. 
 * Desventaja: No puedo poner los atributos concretos como not null.
 * Investigar: DISCRIMINATOR
 *
 */
public class MainSingleTable {

	private static Logger logger = LogManager.getLogger(MainSingleTable.class);

	public static void main(String[] args) {

		Cuenta cd = new CuentaDebito("Ciro Pertussi", new BigDecimal(24500), new BigDecimal(400.95));
		Cuenta cc = new CuentaCredito("Andres Ciro", new BigDecimal(38900), new BigDecimal(679.12));
	
		Session session = HibernateUtil.getSessionFactory().openSession();

		try {
			session.beginTransaction();
			session.persist(cd);
			session.persist(cc);
			session.getTransaction().commit();
			Query query = session.createQuery("from Cuenta"); 
			@SuppressWarnings("unchecked")
			List<Cuenta> cuentas = query.getResultList();
			cuentas.stream().forEach( c -> logger.info(c.getTitular()) );
		} catch (Exception e) {
			e.printStackTrace();
			session.getTransaction().rollback();
		} finally {
			session.close();
		}

		HibernateUtil.getSessionFactory().close();
	}

}
