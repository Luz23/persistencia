package com.ciu.hibernate.inheritance.joinedTable;

import java.math.BigDecimal;
import java.util.List;

import javax.persistence.Query;

import org.apache.logging.log4j.LogManager;
import org.apache.logging.log4j.Logger;
import org.hibernate.Session;

import com.ciu.hibernate.util.HibernateUtil;

/**
 * 
 * Descripcion: Esta estrategia mapea cada clase con su respectiva clase, lo que requiere realizar joins.
 * Observaciones: Ver como resuelve el query polimorfico y concreto.
 *                Las PK de las tablas (sub-clases) son tambien una FK a la tabla super-clase y se describe con: @PrimaryKeyJoinColumns.
 * Desventaja: Rquiere varios joins, y son muchas subclases mas aun.
 * 
 *
 */
public class MainJoinedTable {

	private static Logger logger = LogManager.getLogger(MainJoinedTable.class);

	public static void main(String[] args) {

		// 1 -> dar sin @PrimaryKeyJoinColumns.
		// 2 -> da con @PrimaryKeyJoinColumns.
		
		Cuenta cd = new CuentaDebito("Ciro Pertussi", new BigDecimal(24500), new BigDecimal(400.95));
		Cuenta cc = new CuentaCredito("Andres Ciro", new BigDecimal(38900), new BigDecimal(679.12));
		
		Session session = HibernateUtil.getSessionFactory().openSession();

		try {
			session.beginTransaction();
			session.persist(cd);
			session.persist(cc);
			session.getTransaction().commit();
			Query query = session.createQuery("from Cuenta");
			@SuppressWarnings("unchecked")
			List<Cuenta> cuentas = query.getResultList();
			cuentas.stream().forEach( c -> logger.info(c.getTitular()) );
		} catch (Exception e) {
			e.printStackTrace();
			session.getTransaction().rollback();
		} finally {
			session.close();
		}

		HibernateUtil.getSessionFactory().close();

	}

}
