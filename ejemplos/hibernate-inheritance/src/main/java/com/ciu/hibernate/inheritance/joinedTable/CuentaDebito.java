package com.ciu.hibernate.inheritance.joinedTable;

import java.math.BigDecimal;

import javax.persistence.Entity;
import javax.persistence.PrimaryKeyJoinColumn;

@Entity(name="CuentaDebito")
@PrimaryKeyJoinColumn(name = "cuenta_id")
public class CuentaDebito extends Cuenta {

		// atributos
	private BigDecimal cargoPorBalanceNegativo;

		// constructores
	public CuentaDebito() {
		super();
	}

	public CuentaDebito(String titular, BigDecimal balance, BigDecimal cargoPorBalanceNegativo) {
		super(titular, balance);
		this.cargoPorBalanceNegativo = cargoPorBalanceNegativo;
	}

		// gets y sets
	public BigDecimal getCargoPorBalanceNegativo() {
		return cargoPorBalanceNegativo;
	}

	public void setCargoPorBalanceNegativo(BigDecimal cargoPorBalanceNegativo) {
		this.cargoPorBalanceNegativo = cargoPorBalanceNegativo;
	}

}
